(function ($) {
  Drupal.behaviors.marketoFormsSave = {

    attach: function(context, settings) {

      formVals = $.cookie('marketoFormVals');
      if (typeof formVals != 'undefined') {
        formVals = JSON.parse(formVals);
        // Populate the form.
        $('.node-marketo-form .webform-component').find(':input:not(:button):not(:checkbox)').each(function() {
          var value = formVals[$(this).attr('id')];
          if (typeof value != 'undefined' && value != '') {
            $(this).val(value);
          }
          else {
            $(this).css('background-color', '#f8fff0');
            $(this).change(function() {
              $(this).css('background-color', 'transparent');
            });
          }
        });
      }
      else {
        formVals = {};
      }

      // Save any changes to the form.
      var options = {path: '/', expires: 365}
      $('.node-marketo-form .webform-component').find(':input:not(:button):not(:checkbox)').change(function() {
        formVals[$(this).attr('id')] = $(this).val();
        $.cookie('marketoFormVals', JSON.stringify(formVals), options);
      });

    }
  };
}(jQuery));
